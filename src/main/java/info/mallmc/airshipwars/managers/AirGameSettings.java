package info.mallmc.airshipwars.managers;

import info.mallmc.ballpit.api.ISettings;

public class AirGameSettings implements ISettings {

  @Override
  public int getMinPlayers() {
    return 4;
  }

  @Override
  public int getMaxPlayers() {
    return 100;
  }

  @Override
  public int getMaxPlayersPerTeam() {
    return 50;
  }

  @Override
  public int getMinPlayersPerTeam() {
    return 2;
  }

  @Override
  public int getGameLength() {
    return -1;
  }

}
