package info.mallmc.airshipwars.managers;

import info.mallmc.airshipwars.AirShipWars;
import info.mallmc.airshipwars.api.AirShipGamePhase;
import info.mallmc.airshipwars.inventorys.AnvilGUI;
import info.mallmc.airshipwars.inventorys.AnvilGUI.AnvilClickEvent;
import info.mallmc.airshipwars.inventorys.AnvilGUI.AnvilSlot;
import info.mallmc.ballpit.api.GamePhase;
import info.mallmc.ballpit.api.player.BallPitPlayer;
import info.mallmc.ballpit.managers.GameStageManger;
import info.mallmc.ballpit.util.CountDownManager;
import info.mallmc.ballpit.util.MallTeam;
import info.mallmc.ballpit.util.MapManger;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.helpers.MathHelper;
import info.mallmc.framework.util.items.ItemUtil;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class AirGameManager extends GameStageManger {

  public static int resourcePhaseTimer = 5 * 60;
  public static int descisionPhaseTimer = 2 * 60;
  public static int battlePhaseStartTimer = 30;
  public static int battlePhaseTimer = 10 * 60;

  public static Player redTeamCaptain;
  public static Player blueTeamCaptain;

  public static String redTeamName;
  public static String blueTeamName;

  public static String redTeamShipName;
  public static String blueTeamShipName;

  public static void setRedTeamName(String redTeamName) {
    AirGameManager.redTeamName = redTeamName;
  }

  public static void setBlueTeamName(String blueTeamName) {
    AirGameManager.blueTeamName = blueTeamName;
  }

  public static void setRedTeamShipName(String redTeamShipName) {
    AirGameManager.redTeamShipName = redTeamShipName;
  }

  public static void setBlueTeamShipName(String blueTeamShipName) {
    AirGameManager.blueTeamShipName = blueTeamShipName;
  }

  @Override
  public void setup() {
    super.setup();
  }

  //Todo fix player being forzen when the game starts
  @Override
  public void startGame() {
    super.startGame();
    for(Player player: Bukkit.getOnlinePlayers()) {
      MallPlayer mp = MallPlayer.getPlayer(player.getUniqueId());
      mp.setFrozen(false);
    }
  }

  @Override
  public void startPreGame() {
    super.startPreGame();
    Messaging.broadcastMessage("airshipwars.prep.startingResource");
    for(Player player: Bukkit.getOnlinePlayers())
    {
      MallPlayer mp = MallPlayer.getPlayer(player.getUniqueId());
      mp.setFrozen(true);
      player.getInventory().clear();
      player.setGameMode(GameMode.SURVIVAL);
      player.getInventory().addItem(ItemUtil.createItem(Material.EMERALD, "&aStore", "&2Click me to open the store"));
      int randomX = MathHelper.randomInt(-256, 256);
      randomX+= 0.5;
      int randomZ = MathHelper.randomInt(-256, 256);
      randomZ+= 0.5;
      int Y = Bukkit.getWorld("plant1").getHighestBlockYAt(new Location(Bukkit.getWorld("world"), randomX, 100,randomZ));
      player.teleport(new Location(Bukkit.getWorld("plant1"), randomX, Y+0.5, randomZ));
      player.getInventory().addItem(ItemUtil.createItem(Material.STONE_PICKAXE), ItemUtil.createItem(Material.STONE_AXE), ItemUtil.createItem(Material.STONE_SPADE), ItemUtil.createItem(Material.WOOD_SWORD, "Wooden Cutlas"));
    }
    GamePhase.setGamePhase(AirShipGamePhase.RESOURCE);
  }

  @Override
  public void stopGame() {
    super.stopGame();
  }

  public static void winGame(MallTeam team){
    GamePhase.setGamePhase(AirShipGamePhase.WIN);
    switch(team.getName()){
      case "Red":{
        Messaging.broadcastMessage("airshipwars.game.gameWin", team.getColor(), redTeamName, blueTeamCaptain.getName(), blueTeamName, redTeamShipName);
        Messaging.sendMessage(MallPlayer.getPlayer(redTeamCaptain.getUniqueId()), "airshipwars.game.shipOption", blueTeamShipName);
        Messaging.sendClickableAndHoveringMessage(MallPlayer.getPlayer(redTeamCaptain.getUniqueId()), "airshipwars.game.shipSell", "airshipwars.game.shipSell", "/ship sell");
        Messaging.sendClickableAndHoveringMessage(MallPlayer.getPlayer(redTeamCaptain.getUniqueId()), "airshipwars.game.shipBlowup", "airshipwars.game.shipBlowup", "/ship blowup");
        //TODO: Fireworks
      }
      case "Blue": {
        Messaging.broadcastMessage("airshipwars.game.gameWin", team.getColor(), blueTeamName, redTeamCaptain.getName(), redTeamName, blueTeamShipName);
        Messaging.sendMessage(MallPlayer.getPlayer(blueTeamCaptain.getUniqueId()), "airshipwars.game.shipOption", redTeamShipName);
        Messaging.sendClickableAndHoveringMessage(MallPlayer.getPlayer(blueTeamCaptain.getUniqueId()), "airshipwars.game.shipSell", "airshipwars.game.shipSell", "/ship sell");
        Messaging.sendClickableAndHoveringMessage(MallPlayer.getPlayer(blueTeamCaptain.getUniqueId()), "airshipwars.game.shipBlowup", "airshipwars.game.shipBlowup", "/ship blowup");
        //TODO: Fireworks
      }
    }
    for(Player p: Bukkit.getOnlinePlayers())
    {
      p.getInventory().clear();
    }
  }

  public static void endCounter(){

  }

  public static void startBattlePhase(){
    //TODO: Somehow figure out the ship lowering of planks
  }

  public static void setupBattlePhase(){
    for(Player player: AirShipWars.getInstance().getTeamRed().getPlayers())
    {
      List<Location> spawnableLocations = MapManger.getCurrentMap().getTeamSpawnLocations().get(
          AirShipGamePhase.BATTLE).get(AirShipWars.getInstance().getTeamRed());
      player.teleport(spawnableLocations.get(MathHelper.randomInt(0, spawnableLocations.size() - 1)));
    }
    for(Player player: AirShipWars.getInstance().getTeamBlue().getPlayers())
    {
      List<Location> spawnableLocations = MapManger.getCurrentMap().getTeamSpawnLocations().get(
          AirShipGamePhase.BATTLE).get(AirShipWars.getInstance().getTeamBlue());
      player.teleport(spawnableLocations.get(MathHelper.randomInt(0, spawnableLocations.size() - 1)));
    }
  }

  public static void setupDescisionPhase(){
    for(Player p: Bukkit.getOnlinePlayers())
    {
      BallPitPlayer ballPitPlayer = BallPitPlayer.getBallPitPlayer(p.getUniqueId());
      if(!ballPitPlayer.isSpectator())
      {
        if(!ballPitPlayer.hasTeam()) {
          ballPitPlayer.setTeam(MallTeam.getSmallestTeam());
        }
      }
    }
    Messaging.broadcastMessage("airshipwars.prep.startingDecision");
    int randomRedCaptain = MathHelper.randomInt(0, AirShipWars.getInstance().getTeamRed().getPlayers().size());
    redTeamCaptain = AirShipWars.getInstance().getTeamRed().getPlayers().get(randomRedCaptain);
    redTeamCaptain.getInventory().setHelmet(new ItemStack(Material.GOLD_BLOCK));
    int randomBlueCaptain = MathHelper.randomInt(0, AirShipWars.getInstance().getTeamBlue().getPlayers().size());
    blueTeamCaptain = AirShipWars.getInstance().getTeamBlue().getPlayers().get(randomBlueCaptain);
    blueTeamCaptain.getInventory().setHelmet(new ItemStack(Material.GOLD_BLOCK));
    Messaging.sendMessage(redTeamCaptain, "airshipwars.decision.captain", ChatColor.RED, "Red");
    Messaging.sendMessage(blueTeamCaptain, "airshipwars.decision.captain", ChatColor.BLUE, "Blue");
    for(int i = 0; i < redTeamCaptain.getInventory().getContents().length;i++){
      ItemStack item = redTeamCaptain.getInventory().getItem(i);
      if(item.getType() == Material.EMERALD && item.hasItemMeta() && item.getItemMeta().hasDisplayName() && ChatColor
          .stripColor(item.getItemMeta().getDisplayName()).equalsIgnoreCase("Store")){
        redTeamCaptain.getInventory().setItem(i, ItemUtil.createItem(Material.DIAMOND_SWORD, ChatColor.GOLD + "Diamond Cutlas", "&2The captains sword!"));
      }
    }
    for(int i = 0; i < blueTeamCaptain.getInventory().getContents().length;i++){
      ItemStack item = blueTeamCaptain.getInventory().getItem(i);
      if(item.getType() == Material.EMERALD && item.hasItemMeta() && item.getItemMeta().hasDisplayName() && ChatColor
          .stripColor(item.getItemMeta().getDisplayName()).equalsIgnoreCase("Store")){
        blueTeamCaptain.getInventory().setItem(i, ItemUtil.createItem(Material.DIAMOND_SWORD, ChatColor.GOLD + "Diamond Cutlas", "&2The captains sword!"));
      }
    }
    GamePhase.setGamePhase(AirShipGamePhase.DESCISION);
    AnvilGUI redTeamName = new AnvilGUI(redTeamCaptain, event -> setTeamName(event, "Red"));
    redTeamName.setSlot(AnvilSlot.INPUT_LEFT, ItemUtil.createItem(Material.NAME_TAG, "&aTeam Name"));
    redTeamName.open();

    AnvilGUI blueTeamName = new AnvilGUI(blueTeamCaptain, event -> setTeamName(event, "Blue"));
    blueTeamName.setSlot(AnvilSlot.INPUT_LEFT, ItemUtil.createItem(Material.NAME_TAG, "&aTeam Name"));
    blueTeamName.open();
  }

  public static void setTeamName(AnvilClickEvent event, String team){
    if (event.getSlot() == AnvilGUI.AnvilSlot.OUTPUT) {
      if (team == "Blue") {
        event.setWillClose(true);
        event.setWillDestroy(true);
        blueTeamName = event.getName();
        AnvilGUI blueTeamShipName = new AnvilGUI(blueTeamCaptain, shipEvent -> setShipName(shipEvent, "Blue"));
        blueTeamShipName.setSlot(AnvilSlot.INPUT_LEFT, ItemUtil.createItem(Material.NAME_TAG, "&aShip Name"));
        blueTeamShipName.open();
      } else if (team == "Red") {
        event.setWillClose(true);
        event.setWillDestroy(true);
        redTeamName = event.getName();
        AnvilGUI redTeamShipName = new AnvilGUI(redTeamCaptain, shipEvent -> setShipName(shipEvent, "Red"));
        redTeamShipName.setSlot(AnvilSlot.INPUT_LEFT, ItemUtil.createItem(Material.NAME_TAG, "&aShip Name"));
        redTeamShipName.open();
      }
    } else {
      event.setWillClose(false);
      event.setWillDestroy(false);
    }
  }

  public static void setShipName(AnvilClickEvent event, String team){
    if (event.getSlot() == AnvilGUI.AnvilSlot.OUTPUT) {
      if (team == "Blue") {
        event.setWillClose(true);
        event.setWillDestroy(true);
        blueTeamShipName = event.getName();
        if(redTeamShipName != null){
          GamePhase.setGamePhase(AirShipGamePhase.BATTLE_PREP);
          setupBattlePhase();
        }
      } else if (team == "Red") {
        event.setWillClose(true);
        event.setWillDestroy(true);
        redTeamShipName = event.getName();
        if(blueTeamShipName !=  null){
          GamePhase.setGamePhase(AirShipGamePhase.BATTLE_PREP);
          setupBattlePhase();
        }
      }
    } else {
      event.setWillClose(false);
      event.setWillDestroy(false);
    }
  }

}
