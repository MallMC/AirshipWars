package info.mallmc.airshipwars.managers;

import info.mallmc.ballpit.managers.ItemManager;
import info.mallmc.framework.util.items.ItemUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class AirItemManger extends ItemManager
{

  @Override
  public ItemStack[] getLobbyItems() {
    ItemStack[] items = super.getLobbyItems();
    items[0] = ItemUtil.createItem(Material.BOOK, "&aHow to play", "&2How to play AirshipWars");
    return items;
  }
}
