package info.mallmc.airshipwars.managers;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import info.mallmc.airshipwars.AirShipWars;
import info.mallmc.airshipwars.api.AirShipGamePhase;
import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.GamePhase;
import info.mallmc.ballpit.api.Map;
import info.mallmc.ballpit.managers.MapHelper;
import info.mallmc.ballpit.util.MallTeam;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.framework.Framework;
import info.mallmc.framework.util.helpers.LocationHelper;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

public class AirMapHelper extends MapHelper
{

    @Override
    public List<Map> getMaps()
    {
        List<Map> maps = new ArrayList<>();
        for(AirMap airMap: getAirMaps())
        {
            World world = Bukkit.createWorld(new WorldCreator("map_" + airMap.getMapName()));
            HashMap<GamePhase, Location> centerHashMap = new HashMap<>();
            centerHashMap.put(
                AirShipGamePhase.BATTLE_PREP, LocationHelper.getDeserializedLocation(world, airMap.getMapCenter()));
            centerHashMap.put(
                AirShipGamePhase.BATTLE, LocationHelper.getDeserializedLocation(world, airMap.getMapCenter()));

            HashMap<MallTeam, List<Location>> teamSpawns = new HashMap<>();
            teamSpawns.put(AirShipWars.getInstance().getTeamBlue(), LocationHelper.getDeserializedLocations(world,airMap.getBlueTeamSpawns()));
            teamSpawns.put(AirShipWars.getInstance().getTeamRed(), LocationHelper.getDeserializedLocations(world, airMap.getRedTeamSpawns()));
            HashMap<GamePhase, HashMap<MallTeam, List<Location>>> teamSpawnss = new HashMap<>();
            teamSpawnss.put(AirShipGamePhase.BATTLE_PREP, teamSpawns);
            teamSpawnss.put(AirShipGamePhase.BATTLE, teamSpawns);
            Map map = new Map(airMap.getMapName(), airMap.getAuthors(), centerHashMap, teamSpawnss, true);
            maps.add(map);
        }
        return maps;
    }


    private List<AirMap> getAirMaps() {
        List<AirMap> maps = new ArrayList<>();
        File mainLocation = BallPit.getInstance().getLobbyWorld().getWorldFolder().getParentFile();
        for (File file : mainLocation.listFiles()) {
            if (file.isDirectory() && file.getName().toLowerCase().startsWith("map_")) {
                try {
                    JsonReader reader = new JsonReader(new FileReader(file.getPath() + "/mapInfo.json"));
                    Gson gson = new Gson();
                    AirMap laserMap = gson.fromJson(reader, AirMap.class);
                    maps.add(laserMap);
                    reader.close();
                } catch (Exception e) {
                    Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(),
                        "Map Loading", "Map Folder has no mapInfo.json / is invailded", e.getMessage());
                }
            }

        }
        return maps;
    }

    public class AirMap
    {
        private String mapName;
        private List<String> authors;
        private String mapCenter;
        private List<String> redTeamSpawns;
        private List<String> blueTeamSpawns;

        public List<String> getAuthors() {
            return authors;
        }

        public String getMapName() {
            return mapName;
        }

        public List<String> getRedTeamSpawns() {
            return redTeamSpawns;
        }

        public String getMapCenter() {
            return mapCenter;
        }

        public List<String> getBlueTeamSpawns() {
            return blueTeamSpawns;
        }
    }

}
