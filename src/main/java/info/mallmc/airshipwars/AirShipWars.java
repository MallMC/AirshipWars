package info.mallmc.airshipwars;

import info.mallmc.airshipwars.api.AirShipGamePhase;
import info.mallmc.airshipwars.events.BattlePhaseEvents;
import info.mallmc.airshipwars.events.CountdownEvents;
import info.mallmc.airshipwars.events.PlayerJoinEvents;
import info.mallmc.airshipwars.events.ResourcePhaseEvents;
import info.mallmc.airshipwars.gamemodes.GameModeNormal;
import info.mallmc.airshipwars.inventorys.AirshipWarsInventorys;
import info.mallmc.airshipwars.managers.AirGameHandler;
import info.mallmc.airshipwars.managers.AirGameManager;
import info.mallmc.airshipwars.managers.AirGameSettings;
import info.mallmc.airshipwars.managers.AirItemManger;
import info.mallmc.airshipwars.managers.AirMapHelper;
import info.mallmc.airshipwars.managers.AirScoreboard;
import info.mallmc.airshipwars.worldgen.AirShipWorldGen;
import info.mallmc.ballpit.api.BallPitGame;
import info.mallmc.ballpit.api.GamePhase;
import info.mallmc.ballpit.api.IBallPitGame;
import info.mallmc.ballpit.api.IGameMode;
import info.mallmc.ballpit.managers.GameStageManger;
import info.mallmc.ballpit.managers.LobbyHelper;
import info.mallmc.ballpit.util.MallTeam;
import info.mallmc.core.api.GameRules;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.event.Listener;

@BallPitGame
public class AirShipWars implements IBallPitGame {


  private static AirShipWars instance;

  private MallTeam teamRed;
  private MallTeam teamBlue;
  private AirGameSettings airGameSettings = null;
  private AirGameManager airGameManager = null;
  private AirMapHelper mapHelper = null;
  private LobbyHelper lobbyHelper = null;
  private AirItemManger itemManager = null;
  private AirScoreboard airScoreboard = null;
  private AirGameHandler airGameHandler = null;

  public static AirShipWars getInstance() {
    return instance;
  }

  @Override
  public String getName() {
    return "AirShipWars";
  }


  @Override
  public void onEnable() {
    instance = this;
    teamRed = new MallTeam("Red", ChatColor.RED);
    teamBlue = new MallTeam( "Blue", ChatColor.BLUE);
    AirShipWorldGen airShipWorldGen = new AirShipWorldGen();
    airShipWorldGen.getPlanet1().getWorldBorder().setCenter(0, 0);
    airShipWorldGen.getPlanet1().getWorldBorder().setSize(512);
    airShipWorldGen.getPlanet1().setGameRuleValue(GameRules.SPECTATORS_GENERATE_CHUNKS.getName(), "false");
    airShipWorldGen.getPlanet1().setTime(6000);
    airShipWorldGen.getPlanet1().setDifficulty(Difficulty.PEACEFUL);
    airShipWorldGen.getPlanet1().setGameRuleValue(GameRules.DO_MOB_SPAWNING.getName(), "false");
    airShipWorldGen.getPlanet1().setDifficulty(Difficulty.HARD);
    airShipWorldGen.getPlanet1().setStorm(false);
    airShipWorldGen.getPlanet1().setThundering(false);
    airShipWorldGen.getPlanet1().setGameRuleValue(GameRules.DO_DAYLIGHT_CYCLE.getName(), "false");
    airShipWorldGen.getPlanet1().setGameRuleValue(GameRules.DO_WEATHER_CYCLE.getName(), "false");
//      airShipWorldGen.getPlanet1().setSpawnLocation(null)
    Bukkit.getServer().getWorlds().add(airShipWorldGen.getPlanet1());
    GamePhase.setGamePhase(AirShipGamePhase.LOBBY);
    ResourcePhaseEvents.registerInteractions();
    AirshipWarsInventorys.registerInventorys();
  }

  @Override
  public List<IGameMode> getGameModes() {
    List<IGameMode> iGameModes = new ArrayList<>();
    iGameModes.add(new GameModeNormal());
    return iGameModes;
  }


  @Override
  public List<Listener> getEvents() {
    return Arrays.asList(new PlayerJoinEvents(), new ResourcePhaseEvents(), new CountdownEvents(), new BattlePhaseEvents());
  }

  @Override
  public void onDisable() {

  }

  @Override
  public AirGameHandler getGameHandler() {
    if(airGameHandler == null) {
      airGameHandler = new AirGameHandler();
    }
    return airGameHandler;
  }


  @Override
  public GameStageManger getGameManger() {
    if(airGameManager == null) {
      airGameManager = new AirGameManager();
    }
    return airGameManager;
  }

  @Override
  public AirMapHelper getMapHelper() {
    if(mapHelper == null) {
      mapHelper = new AirMapHelper();
    }
    return mapHelper;
  }


  @Override
  public LobbyHelper getLobbyHelper() {
    if(lobbyHelper == null) {
      lobbyHelper = new LobbyHelper();
    }
    return lobbyHelper;
  }

  @Override
  public AirScoreboard getScoreboards() {
    if(airScoreboard == null)
    {
      airScoreboard = new AirScoreboard();
    }
    return airScoreboard;
  }

  @Override
  public AirItemManger getItemManger() {
    if(itemManager == null) {
      itemManager = new AirItemManger();
    }
    return itemManager;
  }

  @Override
  public AirGameSettings getSettings() {
    if(airGameSettings == null) {
      airGameSettings = new AirGameSettings();
    }
    return airGameSettings;
  }

  public MallTeam getTeamRed() {
    return teamRed;
  }

  public MallTeam getTeamBlue() {
    return teamBlue;
  }
}
