package info.mallmc.airshipwars.database;

import info.mallmc.airshipwars.api.AirShipPlayer;
import info.mallmc.core.MallCore;
import java.util.List;
import org.bukkit.entity.Player;
import org.mongodb.morphia.Datastore;

public class AirPlayerData {

  private static AirPlayerData airPlayerData;

  public static AirPlayerData getInstance() {
    if (airPlayerData == null) {
      airPlayerData = new AirPlayerData();
    }
    return airPlayerData;
  }


  private Datastore datastore = MallCore.getInstance().getDatabase().getDatastore();

  public synchronized boolean isInDatabase(Player player) {
    final List<AirShipPlayer> players = datastore.createQuery(AirShipPlayer.class).field("uuid")
        .equal(player.getUniqueId()).asList();
    if (players.size() > 0 && players.size() == 1) {
      return true;
    } else {
      return false;
    }
  }

  public synchronized void addUser(Player player) {
    AirShipPlayer airShipPlayer = new AirShipPlayer(player.getUniqueId(), 0, 0, 0, 0, 0, 0, 0);
    datastore.save(airShipPlayer);
  }


  public synchronized AirShipPlayer getUser(Player player) {
    final List<AirShipPlayer> players = datastore.createQuery(AirShipPlayer.class).field("uuid")
        .equal(player.getUniqueId()).asList();
    if (players.size() > 0 && players.size() == 1) {
      return players.get(0);
    } else {
      return null;
    }
  }

  public synchronized void updateUser(AirShipPlayer airShipPlayer) {
    final List<AirShipPlayer> players = datastore.createQuery(AirShipPlayer.class).field("uuid")
        .equal(airShipPlayer.getUuid()).asList();
    if (players.size() == 0) {
      return;
    }
    AirShipPlayer foundPlayer = players.get(0);
    airShipPlayer.setId(foundPlayer.getId());
    datastore.save(airShipPlayer);
  }
}
