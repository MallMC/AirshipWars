package info.mallmc.airshipwars.api;

import info.mallmc.ballpit.api.GamePhase;

public class AirShipGamePhase {

  public static final GamePhase LOBBY = new GamePhase("Lobby");
  public static final GamePhase RESOURCE = new GamePhase("Resource");
  public static final GamePhase DESCISION = new GamePhase("Descision");
  public static final GamePhase BATTLE_PREP = new GamePhase("BattlePrep");
  public static final GamePhase BATTLE = new GamePhase("BattleStart");
  public static final GamePhase WIN = new GamePhase("Win");

}
