package info.mallmc.airshipwars.api;

import info.mallmc.airshipwars.database.AirPlayerData;
import java.util.HashMap;
import java.util.UUID;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;

@Entity("airshipPlayers")
public class AirShipPlayer
{
  private static HashMap<String, AirShipPlayer> players = new HashMap<>();

  public static AirShipPlayer getAirShipPlayer(Player p) {
    if (!players.containsKey(p.getName())) {
      if(!AirPlayerData.getInstance().isInDatabase(p))
      {
        AirPlayerData.getInstance().addUser(p);
        return AirPlayerData.getInstance().getUser(p);
      }else
      {
        return AirPlayerData.getInstance().getUser(p);
      }
    }
    return players.get(p.getName());
  }

  public static void removePlayer(Player p) {
    if (players.containsKey(p.getName())) {
      players.remove(p.getName());
    }
  }
  @Id
  private ObjectId id;
  @Indexed(options = @IndexOptions(unique = true))
  private UUID uuid;
  private int pirateCoins;
  private int gamesPlayed;
  private int gamesWon;
  private int timesPlayedCaptain;
  private int timesWonAsCaptain;
  private int kills;
  private int deaths;



  public AirShipPlayer() {
    this.pirateCoins = 0;
    this.gamesPlayed = 0;
    this.gamesWon = 0;
    this.timesPlayedCaptain = 0;
    this.timesWonAsCaptain = 0;
    this.kills = 0;
    this.deaths = 0;
  }

  public AirShipPlayer(UUID uuid, int pirateCoins, int gamesPlayed, int gamesWon, int timesPlayedCaptain, int timesWonAsCaptain, int kills,int deaths)
  {
    this.uuid = uuid;
    this.pirateCoins = pirateCoins;
    this.gamesPlayed = gamesPlayed;
    this.gamesWon = gamesWon;
    this.timesWonAsCaptain = timesWonAsCaptain;
    this.timesPlayedCaptain = timesPlayedCaptain;
    this.kills = kills;
    this.deaths = deaths;
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public int getPirateCoins() {
    return pirateCoins;
  }

  public void setPirateCoins(int pirateCoins) {
    this.pirateCoins = pirateCoins;
  }

  public int getGamesPlayed() {
    return gamesPlayed;
  }

  public int getGamesWon() {
    return gamesWon;
  }

  public void setGamesWon(int gamesWon) {
    this.gamesWon = gamesWon;
  }

  public int getTimesPlayedCaptain() {
    return timesPlayedCaptain;
  }

  public void setTimesPlayedCaptain(int timesPlayedCaptain) {
    this.timesPlayedCaptain = timesPlayedCaptain;
  }

  public int getKills() {
    return kills;
  }

  public void setKills(int kills) {
    this.kills = kills;
  }

  public UUID getUuid() {
    return uuid;
  }

  public int getTimesWonAsCaptain() {
    return timesWonAsCaptain;
  }

  public void setTimesWonAsCaptain(int ttimesWonAsCaptain) {
    this.timesWonAsCaptain = ttimesWonAsCaptain;
  }

  public int getDeaths() {
    return deaths;
  }

  public void setDeaths(int deaths) {
    this.deaths = deaths;
  }
  public Player getPlayer()
  {
    return Bukkit.getPlayer(uuid);
  }
}
