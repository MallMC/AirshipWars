package info.mallmc.airshipwars.worldgen;

import org.bukkit.Server;
import org.bukkit.World;

public class WorldLoadRunnable implements Runnable{

  private transient Server server = null;
  private transient World world = null;
  private transient int taskID = -1;
  private transient int chunksPerRun = 1;
  private transient int amountOfChunks = 1;

  private transient int x = 0;
  private transient int z = 0;
  private transient int current = 0;
  private transient int lastX = 0;
  private transient int lastZ = 0;

  public WorldLoadRunnable(Server server, World world, int chunksPerRun, int amountOfChunks) {
    this.server = server;
    this.world = world;
    this.chunksPerRun = chunksPerRun;
    this.amountOfChunks  = amountOfChunks;
  }


  @Override
  public void run() {
    for(int loop = 0; loop < chunksPerRun; loop++){
      world.loadChunk(x, z, true);
      lastX = x;
      lastZ = z;
      x = x + 16;
      z = z + 16;
    }
  }
}
