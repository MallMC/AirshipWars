package info.mallmc.airshipwars.worldgen;

import info.mallmc.ballpit.BallPit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.generator.ChunkGenerator;

public class AirShipWorldGen extends ChunkGenerator
{
  World planet1;

  public AirShipWorldGen()
  {
    WorldCreator worldCreator = new WorldCreator("plant1");
    worldCreator = worldCreator.type(WorldType.CUSTOMIZED);
    worldCreator = worldCreator.generateStructures(false);
    worldCreator = worldCreator.generatorSettings("{\"coordinateScale\":684.412,\"heightScale\":684.412,\"lowerLimitScale\":512.0,\"upperLimitScale\":512.0,\"depthNoiseScaleX\":200.0,\"depthNoiseScaleZ\":200.0,\"depthNoiseScaleExponent\":0.5,\"mainNoiseScaleX\":1000.0,\"mainNoiseScaleY\":3000.0,\"mainNoiseScaleZ\":1000.0,\"baseSize\":8.5,\"stretchY\":10.0,\"biomeDepthWeight\":1.0,\"biomeDepthOffset\":0.0,\"biomeScaleWeight\":1.0,\"biomeScaleOffset\":0.0,\"seaLevel\":1,\"useCaves\":true,\"useDungeons\":false,\"dungeonChance\":1,\"useStrongholds\":false,\"useVillages\":false,\"useMineShafts\":false,\"useTemples\":false,\"useMonuments\":false,\"useMansions\":false,\"useRavines\":true,\"useWaterLakes\":true,\"waterLakeChance\":100,\"useLavaLakes\":false,\"lavaLakeChance\":100,\"useLavaOceans\":false,\"fixedBiome\":1,\"biomeSize\":1,\"riverSize\":1,\"dirtSize\":33,\"dirtCount\":10,\"dirtMinHeight\":0,\"dirtMaxHeight\":256,\"gravelSize\":33,\"gravelCount\":8,\"gravelMinHeight\":0,\"gravelMaxHeight\":256,\"graniteSize\":1,\"graniteCount\":0,\"graniteMinHeight\":0,\"graniteMaxHeight\":0,\"dioriteSize\":1,\"dioriteCount\":0,\"dioriteMinHeight\":0,\"dioriteMaxHeight\":0,\"andesiteSize\":1,\"andesiteCount\":0,\"andesiteMinHeight\":0,\"andesiteMaxHeight\":0,\"coalSize\":25,\"coalCount\":20,\"coalMinHeight\":0,\"coalMaxHeight\":128,\"ironSize\":25,\"ironCount\":20,\"ironMinHeight\":0,\"ironMaxHeight\":64,\"goldSize\":18,\"goldCount\":2,\"goldMinHeight\":0,\"goldMaxHeight\":32,\"redstoneSize\":16,\"redstoneCount\":8,\"redstoneMinHeight\":0,\"redstoneMaxHeight\":16,\"diamondSize\":12,\"diamondCount\":1,\"diamondMinHeight\":0,\"diamondMaxHeight\":16,\"lapisSize\":8,\"lapisCount\":1,\"lapisCenterHeight\":16,\"lapisSpread\":16}");
    this.planet1 = BallPit.getInstance().getServer().createWorld(worldCreator);
//    Bukkit.getScheduler().scheduleSyncRepeatingTask(BallPit.getInstance(), new WorldLoadRunnable(BallPit.getInstance().getServer(), this.planet1, 10, 512 / 16), 10, (512/16)/ 10);
  }

  public World getPlanet1() {
    return planet1;
  }
}
