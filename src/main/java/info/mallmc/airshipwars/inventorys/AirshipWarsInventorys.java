package info.mallmc.airshipwars.inventorys;

import info.mallmc.airshipwars.inventorys.store.CategoryTemplateInventory;
import info.mallmc.framework.util.registies.InventoryRegistry;
import java.util.HashMap;

public class AirshipWarsInventorys {

  private static StoreInventory storeInventory = new StoreInventory();
  private static HashMap<Category, CategoryTemplateInventory> storeInventorys = new HashMap<>();

  public static void registerInventorys(){
    for(Category category: Category.values())
    {
      CategoryTemplateInventory categoryTemplateInventory = new CategoryTemplateInventory(category);
      storeInventorys.put(category, categoryTemplateInventory);
      InventoryRegistry.registerInventory(categoryTemplateInventory);
    }
    InventoryRegistry.registerInventory(storeInventory);
  }

  public static StoreInventory getStoreInventory() {
    return storeInventory;
  }

  public static CategoryTemplateInventory getCategoryInventory(Category category)
  {
    return storeInventorys.get(category);
  }
}
