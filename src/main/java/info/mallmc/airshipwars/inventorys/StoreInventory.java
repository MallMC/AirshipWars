package info.mallmc.airshipwars.inventorys;

import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.items.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class StoreInventory extends MallInventory {

  @Override
  public String getName() {
    return ChatColor.GREEN +"Store";
  }

  @Override
  public void open(Player mallPlayer) {
    Inventory inventory = Bukkit.createInventory(mallPlayer, 9, Messaging.colorizeMessage(getName()));
    for(Category category: Category.values()) {
      inventory.setItem(category.getInventorySpot(), category.getItemStack());
    }
    mallPlayer.openInventory(inventory);
  }

  @Override
  public void click(Player mallPlayer, int i) {
    for (Category category : Category.values()) {
      if (category.getInventorySpot() == i) {
        AirshipWarsInventorys.getCategoryInventory(category).open(mallPlayer);
      }
    }
  }
}
