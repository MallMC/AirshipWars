package info.mallmc.airshipwars.inventorys.store;

import info.mallmc.airshipwars.inventorys.Category;
import info.mallmc.airshipwars.inventorys.ShopItems;
import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.items.ItemUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class CategoryTemplateInventory extends MallInventory {

  private String name;
  private Category category;

  public CategoryTemplateInventory(Category category) {
    this.name = category.getName();
    this.category = category;
  }

  @Override
  public String getName() {
    return ChatColor.GREEN + name;
  }

  @Override
  public void open(Player mallPlayer) {
    HashMap<Integer, List<ItemStack>> rows = new HashMap<>();
    for(ShopItems shopItems : ShopItems.values()){
      if(shopItems.getCategory() == category){
        rows.computeIfAbsent(shopItems.getRow(), row -> rows.put(row, new ArrayList<>()));
        rows.get(shopItems.getRow()).add(shopItems.getItem());
      }
    }

    int inventorySize = (rows.size() + 2) * 9;
    if(inventorySize > 54){
      //TODO: Page system
      return;
    }

    Inventory inventory = Bukkit
        .createInventory(mallPlayer, inventorySize, Messaging.colorizeMessage(getName()));
    for(int row : rows.keySet()){
      for(ItemStack itemStack : rows.get(row)){
        int itemPosition = (row + 1)*9 + rows.get(row).indexOf(itemStack);
        int remainderSlots = 9 - rows.get(row).size();
        if(remainderSlots % 2 == 0){
          itemPosition += (remainderSlots / 2);
        }

       inventory.setItem(itemPosition, itemStack);
      }
    }

    mallPlayer.openInventory(inventory);
  }

  @Override
  public void click(Player mallPlayer, int i)
  {
    HashMap<Integer, List<ShopItems>> rows = new HashMap<>();
    for(ShopItems shopItems : ShopItems.values()){
      if(shopItems.getCategory() == category){
        rows.computeIfAbsent(shopItems.getRow(), row -> rows.put(row, new ArrayList<>()));
        rows.get(shopItems.getRow()).add(shopItems);
      }
    }
    for(int row : rows.keySet()){
      for(ShopItems shopItem : rows.get(row)){
        int itemPosition = (row + 1)*9 + rows.get(row).indexOf(shopItem);
        int remainderSlots = 9 - rows.get(row).size();
        if(remainderSlots % 2 == 0){
          itemPosition += (remainderSlots / 2);
        }
        if(itemPosition == i) {
          Player player = Bukkit.getPlayer(mallPlayer.getUniqueId());
          if(player.getInventory().contains(shopItem.getCurrency().getMaterial(), shopItem.getPrice())) {
            player.getInventory().addItem(shopItem.getItem());
            //Todo make it work no matter what stack the currency is right now it only takes it away if you have the amount you need
            //Todo check if inventory is full
            player.getInventory()
                .remove(ItemUtil.createStack(shopItem.getCurrency().getMaterial(), shopItem.getPrice()));
            Messaging.sendMessage(mallPlayer, "airshipwars.game.inventory.category.purchased");
          }else
          {
            Messaging.sendMessage(mallPlayer, "airshipwars.game.inventory.category.needMore", shopItem.getCurrency().getName(), shopItem.getPrice());
          }
        }
      }
    }

  }
}
