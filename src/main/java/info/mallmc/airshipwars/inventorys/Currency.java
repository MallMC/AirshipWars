package info.mallmc.airshipwars.inventorys;

import org.bukkit.Material;

public enum Currency {

  IRON("Iron", Material.IRON_INGOT),
  COAL("Coal", Material.COAL),
  GOLD("Gold", Material.GOLD_INGOT),
  EMERALD("Emerald", Material.EMERALD),
  DIAMOND("Diamond", Material.DIAMOND),
  LAPIS("Lapis", Material.INK_SACK),
  STONE("Stone", Material.STONE);

  private final String name;
  private final Material material;

  Currency(String name, Material material){
    this.name = name;
    this.material = material;
  }

  public String getName() {
    return name;
  }

  public Material getMaterial() {
    return material;
  }
}
