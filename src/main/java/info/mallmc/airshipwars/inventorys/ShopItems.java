package info.mallmc.airshipwars.inventorys;

import info.mallmc.framework.util.items.ItemUtil;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum ShopItems {
  STONE_PICKAXE(Material.STONE_PICKAXE, "&aStone Pickaxe", 3, Currency.STONE, Category.TOOLS, 0, "&2A simple stone pickaxe"),
  STONE_AXE(Material.STONE_AXE, "&aStone Axe", 3, Currency.STONE, Category.TOOLS, 1, "&2A simple stone axe"),
  STONE_SPADE(Material.STONE_SPADE, "&aStone Spade", 3, Currency.STONE, Category.TOOLS, 2, "&2A simple stone spade"),
  STONE_HOE(Material.STONE_HOE, "&aStone Hoe", 3, Currency.STONE, Category.TOOLS, 3, "&2A simple stone hoe"),
  IRON_PICKAXE(Material.IRON_PICKAXE, "&aIron Pickaxe", 3, Currency.IRON, Category.TOOLS, 0, "&2A simple iron pickaxe"),
  IRON_AXE(Material.IRON_AXE, "&aIron Axe", 3, Currency.IRON, Category.TOOLS, 1, "&2A simple iron axe"),
  IRON_SPADE(Material.IRON_SPADE, "&aIron Spade", 3, Currency.IRON, Category.TOOLS, 2, "&2A simple iron spade"),
  IRON_HOE(Material.IRON_HOE, "&aIron Hoe", 3, Currency.IRON, Category.TOOLS, 3, "&2A simple iron hoe"),
  GOLD_PICKAXE(Material.GOLD_PICKAXE, "&aGold Pickaxe", 3, Currency.IRON, Category.TOOLS, 0, "&2A simple gold pickaxe"),
  GOLD_AXE(Material.GOLD_AXE, "&aGold Axe", 3, Currency.GOLD, Category.TOOLS, 1, "&2A simple gold axe"),
  GOLD_SPADE(Material.GOLD_SPADE, "&aGold Spade", 3, Currency.GOLD, Category.TOOLS, 2, "&2A simple gold spade"),
  GOLD_HOE(Material.GOLD_HOE, "&aGold Hoe", 3, Currency.GOLD, Category.TOOLS, 3, "&2A simple gold hoe"),
  DIAMOND_PICKAXE(Material.DIAMOND_PICKAXE, "&aDiamond Pickaxe", 3, Currency.DIAMOND, Category.TOOLS, 0, "&2A simple diamond pickaxe"),
  DIAMOND_AXE(Material.DIAMOND_AXE, "&aDiamond Axe", 3, Currency.DIAMOND, Category.TOOLS, 1, "&2A simple diamond axe"),
  DIAMOND_SPADE(Material.DIAMOND_SPADE, "&aDiamond Spade", 3, Currency.DIAMOND, Category.TOOLS, 2, "&2A simple diamond spade"),
  DIAMOND_HOE(Material.DIAMOND_HOE, "&aDiamond Hoe", 3, Currency.DIAMOND, Category.TOOLS, 3, "&2A simple diamond hoe");

  private Material material;
  private String name;
  private List<String> lore;
  private int price;
  private Currency currency;
  private Category category;
  private int row;

  ShopItems(Material material, String name, int price, Currency currency,
      Category category, int row, String... lore) {
    this.material = material;
    this.name = name;
    this.lore = new LinkedList<>(Arrays.asList(lore));
    this.price = price;
    this.currency = currency;
    this.category = category;
    this.row = row;
    this.lore.add("&6" +price + " " + currency.getName());
    this.lore.add("&2Click to purchase");
  }

  public Material getMaterial() {
    return material;
  }

  public String getName() {
    return name;
  }

  public List<String> getLore() {
    return lore;
  }

  public int getPrice() {
    return price;
  }

  public Category getCategory() {
    return category;
  }

  public int getRow() {
    return row;
  }

  public ItemStack getItem(){
    return ItemUtil.createItem(material, name, lore);
  }

  public Currency getCurrency() {
    return currency;
  }
}
