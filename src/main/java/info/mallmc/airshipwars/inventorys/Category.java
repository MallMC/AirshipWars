package info.mallmc.airshipwars.inventorys;

import info.mallmc.framework.util.items.ItemUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum Category {

  TOOLS("Tools", 1, ItemUtil.createItem(Material.IRON_PICKAXE, "&9Tools", "&2Purchase some tools to use")),
  WEAPONS("Weapons", 2, ItemUtil.createItem(Material.IRON_SWORD, "&9Weapons", "&2Purchase some weapons to use")),
  BLOCKS("Blocks", 3, ItemUtil.createItem(Material.SANDSTONE, "&9Blocks", "&2Purchase some blocks to build with")),
  FOOD("Food", 4, ItemUtil.createItem(Material.CAKE, "&9Food", "&2Purchase some food to eat, nom nom nom.")),
  ARMOUR("Armour", 5, ItemUtil.createItem(Material.GOLD_CHESTPLATE, "&9Armour", "&2Purchase some armour to wear")),
  ARCHERY("Archery", 6, ItemUtil.createItem(Material.BOW, "&9Bows", "&2 Pew Pew, shoot someone from far away")),
  UTIL("Util", 7, ItemUtil.createItem(Material.TNT, "&9Utility", "&2Purchase some utility blocks to help you out"));

  private final String name;
  private final int inventorySpot;
  private final ItemStack itemStack;

  Category(String name, int inventorySpot, ItemStack itemStack)
  {
    this.name = name;
    this.inventorySpot = inventorySpot;
    this.itemStack = itemStack;
  }

  public String getName() {
    return name;
  }

  public int getInventorySpot() {
    return inventorySpot;
  }

  public ItemStack getItemStack() {
    return itemStack;
  }
}
