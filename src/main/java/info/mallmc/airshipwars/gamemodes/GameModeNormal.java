package info.mallmc.airshipwars.gamemodes;

import info.mallmc.ballpit.api.IGameMode;
import info.mallmc.core.api.GameState;

public class GameModeNormal implements IGameMode {

  @Override
  public String getName() {
    return "TDM";
  }

  @Override
  public void onRegister() {

  }

  @Override
  public void oneMinute(GameState gameState) {

  }

  @Override
  public void oneSecond(GameState gameState) {

  }

}
