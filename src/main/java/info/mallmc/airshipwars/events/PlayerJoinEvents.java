package info.mallmc.airshipwars.events;

import info.mallmc.airshipwars.api.AirShipPlayer;
import info.mallmc.framework.util.items.FrameworkItems;
import info.mallmc.framework.util.items.ItemUtil;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerJoinEvents implements Listener
{
    @EventHandler
    public void playerLoginEvent(PlayerLoginEvent event)
    {
      AirShipPlayer airShipPlayer = AirShipPlayer.getAirShipPlayer(event.getPlayer());

    }
    @EventHandler
    public void playerJoinEvent(PlayerJoinEvent event)
    {
      AirShipPlayer airShipPlayer = AirShipPlayer.getAirShipPlayer(event.getPlayer());
      event.getPlayer().getInventory().clear();
      event.getPlayer().setFoodLevel(20);
      event.getPlayer().setHealth(20);
      event.getPlayer().setGameMode(GameMode.ADVENTURE);
    }
    @EventHandler
    public void playerLeaveEvent(PlayerQuitEvent event)
    {
      AirShipPlayer.removePlayer(event.getPlayer());
    }
}
