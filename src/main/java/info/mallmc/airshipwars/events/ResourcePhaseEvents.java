package info.mallmc.airshipwars.events;

import info.mallmc.airshipwars.api.AirShipGamePhase;
import info.mallmc.airshipwars.inventorys.AirshipWarsInventorys;
import info.mallmc.ballpit.api.GamePhase;
import info.mallmc.core.api.GameState;
import info.mallmc.framework.interactions.Interaction;
import info.mallmc.framework.interactions.InteractionRegistry;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ResourcePhaseEvents implements Listener {

  @EventHandler
  public void onBlockBreak(BlockBreakEvent event){
    if(GamePhase.getGamePhase() == AirShipGamePhase.RESOURCE){
      Player player = event.getPlayer();
      Block block = event.getBlock();
      if(player.getInventory().getItemInMainHand().getType() == Material.IRON_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.WOOD_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.STONE_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.GOLD_PICKAXE || player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_PICKAXE){
        switch(block.getType()){
          case IRON_ORE:
            event.setDropItems(false);
            block.breakNaturally();
            player.getWorld().dropItem(block.getLocation(), new ItemStack(Material.IRON_INGOT, 1));
            break;
          case GOLD_ORE:
            event.setDropItems(false);
            block.breakNaturally();
            player.getWorld().dropItem(block.getLocation(), new ItemStack(Material.GOLD_INGOT, 1));
            break;
          case STONE:
            event.setDropItems(false);
            block.breakNaturally();
            player.getWorld().dropItem(block.getLocation(), new ItemStack(Material.STONE, 1));
            break;
        }
      }
    }else if(GamePhase.getGamePhase() != AirShipGamePhase.BATTLE) {
      event.setCancelled(true);
    }
  }

  @EventHandler
  public void onDropItemEvent(PlayerDropItemEvent event){
    if(GamePhase.getGamePhase() == AirShipGamePhase.RESOURCE) {
      if (event.getItemDrop().getItemStack().getType() == Material.EMERALD) {
        if (event.getItemDrop().getItemStack().hasItemMeta() && event.getItemDrop().getItemStack()
            .getItemMeta().hasDisplayName()) {
          if (ChatColor
              .stripColor(event.getItemDrop().getItemStack().getItemMeta().getDisplayName())
              .equalsIgnoreCase("Store")) {

          }
        }
      }
    }else if(GamePhase.getGamePhase() == AirShipGamePhase.DESCISION || GamePhase.getGamePhase() == AirShipGamePhase.LOBBY){
      event.setCancelled(true);
    }


  }

  @EventHandler
  public void onPlayerDamage(EntityDamageEvent event){
    if(event.getEntity() instanceof Player){
      if(GameState.getGameState() == GameState.LOBBY || GamePhase.getGamePhase() == AirShipGamePhase.RESOURCE || GamePhase.getGamePhase() == AirShipGamePhase.LOBBY || GamePhase.getGamePhase() == AirShipGamePhase.BATTLE){
        event.setCancelled(true);
      }
    }
  }

  public static void registerInteractions(){
    InteractionRegistry.registerInteraction(Interaction.RIGHT_CLICK, (event) -> handleInventorys(event));
  }

  public static void handleInventorys(Event event){
    PlayerInteractEvent playerInteractEvent = (PlayerInteractEvent) event;
    Player player = playerInteractEvent.getPlayer();
    if (playerInteractEvent.getItem() != null && playerInteractEvent.getItem().hasItemMeta() && playerInteractEvent.getItem().getItemMeta().hasDisplayName()) {
      switch (ChatColor.stripColor(playerInteractEvent.getItem().getItemMeta().getDisplayName().toLowerCase()))
      {
        case "store":
          AirshipWarsInventorys.getStoreInventory().open(player);
          break;
      }
    }
  }


}
