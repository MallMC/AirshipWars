package info.mallmc.airshipwars.events;

import info.mallmc.airshipwars.api.AirShipGamePhase;
import info.mallmc.airshipwars.managers.AirGameManager;
import info.mallmc.ballpit.api.GamePhase;
import info.mallmc.ballpit.api.player.BallPitPlayer;
import info.mallmc.ballpit.util.MallTeam;
import info.mallmc.framework.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class BattlePhaseEvents implements Listener {

  @EventHandler
  public void onPlayerDeath(PlayerDeathEvent event){
    if(GamePhase.getGamePhase() == AirShipGamePhase.BATTLE){
      BallPitPlayer ballPitPlayerKilled = BallPitPlayer.getBallPitPlayer(event.getEntity().getUniqueId());
      BallPitPlayer ballPitPlayerKiller = BallPitPlayer.getBallPitPlayer(event.getEntity().getKiller().getUniqueId());
     if(event.getEntity().getUniqueId() == AirGameManager.redTeamCaptain.getUniqueId() || event.getEntity().getUniqueId() == AirGameManager.blueTeamCaptain.getUniqueId()){
       AirGameManager.winGame(ballPitPlayerKilled.getTeam());
       event.setDeathMessage(null);
       event.setKeepInventory(false);
       ChatColor killerColor = ballPitPlayerKiller.getTeam().getColor();
       ChatColor killedColor = ballPitPlayerKilled.getTeam().getColor();
       Messaging.broadcastMessage("airshipwars.game.kill", killerColor, event.getEntity().getKiller().getName(),killedColor, event.getEntity().getName());
     }else{
       event.setDeathMessage(null);
       event.setKeepInventory(false);
       ChatColor killerColor = ballPitPlayerKiller.getTeam().getColor();
       ChatColor killedColor = ballPitPlayerKilled.getTeam().getColor();
       Messaging.broadcastMessage("airshipwars.game.kill", killerColor, event.getEntity().getKiller().getName(),killedColor, event.getEntity().getName());
     }
    }
  }

  @EventHandler
  public void onPlayerDamage(EntityDamageByEntityEvent event){
    if(event.getEntity() instanceof Player){
      Player damagedPlayer = (Player)event.getEntity();
      BallPitPlayer ballPitdamagedPlayer = BallPitPlayer.getBallPitPlayer(damagedPlayer.getUniqueId());
      if(event.getDamager() instanceof Player){
        Player damagerPlayer = (Player)event.getDamager();
        BallPitPlayer ballPitPlayerdamager = BallPitPlayer.getBallPitPlayer(damagedPlayer.getUniqueId());
        if(GamePhase.getGamePhase() == AirShipGamePhase.BATTLE){
          MallTeam damagedPlayerTeam = ballPitdamagedPlayer.getTeam();
          MallTeam damagerPlayerTeam = ballPitPlayerdamager.getTeam();
          if(damagedPlayerTeam.getName() == damagerPlayerTeam.getName()){
            event.setCancelled(true);
          }
        }
      }

    }
  }
}
