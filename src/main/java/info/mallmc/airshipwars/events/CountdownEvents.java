package info.mallmc.airshipwars.events;

import info.mallmc.airshipwars.api.AirShipGamePhase;
import info.mallmc.airshipwars.managers.AirGameManager;
import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.GamePhase;
import info.mallmc.ballpit.api.player.BallPitPlayer;
import info.mallmc.ballpit.util.CountdownHelper;
import info.mallmc.ballpit.util.EnumDisplaySpot;
import info.mallmc.framework.events.custom.OneSecondEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class CountdownEvents implements Listener {

  @EventHandler
  public void onOneSecondCountdown(OneSecondEvent event){
    if(GamePhase.getGamePhase() == AirShipGamePhase.RESOURCE){
      if(AirGameManager.resourcePhaseTimer > 0){
        AirGameManager.resourcePhaseTimer--;
        CountdownHelper.display(EnumDisplaySpot.BOSSBAR, AirGameManager.resourcePhaseTimer);
      }else{
        GamePhase.setGamePhase(AirShipGamePhase.DESCISION);
        for(Player player : Bukkit.getOnlinePlayers()){
          if(!BallPitPlayer.getBallPitPlayer(player.getUniqueId()).isSpectator()) {
            player.teleport(BallPit.getInstance().getLoadedGame().getLobbyHelper().getLobbySpawnLocation());
          }
        }
        AirGameManager.setupDescisionPhase();
      }
    }else if(GamePhase.getGamePhase() == AirShipGamePhase.DESCISION){
      if(AirGameManager.descisionPhaseTimer > 0){
        AirGameManager.descisionPhaseTimer--;
        CountdownHelper.display(EnumDisplaySpot.BOSSBAR, AirGameManager.descisionPhaseTimer);
      }else{
        GamePhase.setGamePhase(AirShipGamePhase.BATTLE_PREP);
        AirGameManager.setupBattlePhase();
      }
    }else if(GamePhase.getGamePhase() == AirShipGamePhase.BATTLE_PREP){
      if(AirGameManager.battlePhaseStartTimer > 0){
        AirGameManager.battlePhaseStartTimer--;
        CountdownHelper.display(EnumDisplaySpot.BOSSBAR, AirGameManager.battlePhaseStartTimer);
        if(AirGameManager.battlePhaseTimer <= 10){
          CountdownHelper.display(EnumDisplaySpot.TITLE, AirGameManager.battlePhaseStartTimer);
          CountdownHelper.display(EnumDisplaySpot.CHAT, AirGameManager.battlePhaseStartTimer);
        }
      }else{
        GamePhase.setGamePhase(AirShipGamePhase.BATTLE);
        AirGameManager.startBattlePhase();
      }
    }else if(GamePhase.getGamePhase() == AirShipGamePhase.BATTLE){
      if(AirGameManager.battlePhaseTimer > 0){
        AirGameManager.battlePhaseTimer--;
        CountdownHelper.display(EnumDisplaySpot.BOSSBAR, AirGameManager.battlePhaseTimer);
      }else{
        GamePhase.setGamePhase(AirShipGamePhase.WIN);
        AirGameManager.endCounter();
      }
    }


  }
}
